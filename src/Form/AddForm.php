<?php

/**
 * @file
 * Contains \Drupal\age_calculator\Form\AddForm.
 */

namespace Drupal\age_calculator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class AddForm.
 *
 * @package Drupal\age_calculator\Form\AddForm
 */
class AddForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'age_calc_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();
    // Birthdate Field definition.
    $form['birthdate'] = array(
      '#title' => t('Date of birth'),
      '#type' => 'date',
      '#weight' => 1,
      '#default_value' => '',
    );
    // Age at the date.
    $form['age_on_date'] = array(
      '#title' => t('Age on date'),
      '#type' => 'date',
      '#weight' => 2,
      '#default_value' => '',
    );
    // Submit button definition.
    $form['submit'] = array(
      '#type' => 'button',
      '#value' => t('Calculate'),
      '#weight' => 3,
      '#ajax'  => array(
        'url' => Url::fromRoute('age_calculator.add'),
      ),
    );
    // Results section markup.
    $form['calculated_age'] = array(
      '#type' => 'markup',
      '#weight' => 4,
      '#prefix' => '<div id="age_calculator_calculated_age">',
      '#suffix' => '</div>',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
