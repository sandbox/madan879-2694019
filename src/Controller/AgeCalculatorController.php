<?php

/**
 * @file
 * Contains \Drupal\age_calculator\Controller\AgeCalculatorController.
 */

namespace Drupal\age_calculator\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class AgeCalculatorController.
 *
 * @package Drupal\age_calculator\Controller
 */
class AgeCalculatorController extends ControllerBase {
  /**
   * Add a favorite.
   */
  public function calculateAge() {
    $output = '';
    if (\Drupal::request()->request->get('birthdate')) {
      // Getting user input.
      $birthdate_array = \explode('-', \Drupal::request()->request->get('birthdate'));
      $age_on_date_array = \explode('-', \Drupal::request()->request->get('age_on_date'));
      // Formatting user input.
      $birthdate = $birthdate_array[2] . '-' . $birthdate_array[1] . '-' . $birthdate_array[0];
      $age_on_date = $age_on_date_array[2] . '-' . $age_on_date_array[1] . '-' . $age_on_date_array[0];
      // Convert dates to timestamps.
      $birthdate_timestamp = strtotime($birthdate);
      $age_on_date_timestamp = strtotime($age_on_date);
      // Check if birthdate greater than age on time.
      if ($birthdate_timestamp <= $age_on_date_timestamp) {
        // Object declaration.
        $birthdate_datetime = new \DateTime($birthdate);
        $age_on_date_datetime = new \DateTime($age_on_date);
        // Including helper functions inc file.
        module_load_include('inc', 'age_calculator', 'age_calculator.helper_functions');
        // Getting output.
        $output = age_calculator_get_results($birthdate_datetime, $age_on_date_datetime);
      }
      else {
        $output = t('ERROR: Age on date should not be lesser than date of birth.');
      }
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#age_calculator_calculated_age', $output));
    return $response;
  }

}
